<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="css.css" type="text/css">

<body>

    <?php
        session_start();
        $db_name = $_SESSION["name"];
        $db_gender = $_SESSION["gender"]-1;
        $db_faculty = $_SESSION["khoa"];
        $db_birthday = $_SESSION["date"];
        $db_address = $_SESSION["address"];
        $db_avartar = $_SESSION["imageInput"];

        $date =  date("d/m/Y", strtotime($_SESSION["date"]));
        $gender = ($db_gender  == 0) ? 'Nữ' : 'Nam';
        $faculty = ($db_faculty  == "MAT") ? 'Khoa học máy tính' : 'Khoa học dữ liệu';

        $conn = new mysqli('localhost','root','','web');
        if (!empty($_POST["submit"])){
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }else{
                $sql = "INSERT INTO student (name,gender,faculty,birthday,address,avartar) VALUES ('".$db_name."',".$db_gender.",'".$db_faculty."','".$db_birthday."','".$db_address."','".$db_avartar."')";
                $conn->query($sql);
            }
            header('Location: complete_regist.php');
        }

    ?>

    <div class="login">
        <form method="POST" action="">
            <div class="row">
                <div class="column_label div_label_confirm">Họ và tên
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<div class="text">' . $_SESSION["name"] . ' </div> '
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Giới tính
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<div class="text">' .  $gender . ' </div> '
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Phân khoa
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <div class="column_input">
                        <?php
                            echo '<div class="text">' . $faculty . ' </div> '
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Ngày sinh
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<div class="text">' . $date . ' </div> '
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Địa chỉ</div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<div class="text">' . $db_address . ' </div> '
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Hình ảnh</div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<img class="text" src=".'.$db_avartar.'" width="50%">'
                    ?>
                </div>
            </div>

            <form method="POST" action="confirm.php">
                <div class="">
                    <input class="center div_button" type="submit" value="Xác nhận" name="submit">
                </div>
            </form>

        </form>

    </div>
</body>